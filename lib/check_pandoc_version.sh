#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

MIN_PANDOC_VERSION='3.1.13'

PANDOC_VERSION=$(pandoc --version | grep -E "pandoc\s[0-9\.]+" | awk '{print $2}') || echo "PANDOC NOT INSTALLED" && exit 1

verlte() {
    [  "$1" = "`echo -e "$1\n$2" | sort -V | head -n1`" ]
}

verlt() {
    [ "$1" = "$2" ] && return 1 || verlte $1 $2
}

verlt $PANDOC_VERSION $MIN_PANDOC_VERSION && echo "PANDOC VERSION NOT OK" && exit 1 || echo "PANDOC VERSION OK"
