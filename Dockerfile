# syntax=docker/dockerfile:1.4
FROM --platform=$BUILDPLATFORM python:3.10-alpine AS builder

WORKDIR /app

ARG DOCKER_UID
ARG DOCKER_GID

RUN apk update && apk add bash curl make sudo

RUN addgroup -g ${DOCKER_GID} app_user && adduser --disabled-password -u ${DOCKER_UID} -s /bin/bash -G app_user app_user

COPY lib/ /app/lib/
COPY Makefile /app
RUN make install_pandoc
COPY requirements.txt /app
RUN --mount=type=cache,target=/root/.cache/pip \
    pip3 install -r requirements.txt
COPY requirements.dev.txt /app
RUN --mount=type=cache,target=/root/.cache/pip \
    pip3 install -r requirements.dev.txt

USER app_user
