# Dokuwiki2BookStack

A script to migrate data from a Dokuwiki installation to a BookStack server.

You need access to Dokuwiki's page's source files and a access to BookStack's API.

## Requirements

- Pandoc version >3.1.13
- Python 3

## Installation

1. Check your `pandoc` installation : `make check_pandoc_version`
2. If Pandoc is not installed or you don't have the minimal required version. Install it: `make install_pandoc`
3. Install `dk2bs`:

```
python3 -m venv .venv
. .venv/bin/activate
pip install .
```

## Prepare

Before importing data into the BookStack instance, you need to generate access tokens to access the API.
We recommend creating a dedicated account to run this import script. It allows to separate data which has been automatically imported.

## Usage

1. Copy the files of your Dokuwiki's instance data in your current directory in a folder named `dokuwiki_data`
2. Edit the environment variables in the `.env` file to setup your BookStack API access
4. Prepare the mapping file: `dk2bs prepare`
4. Edit the `dokuwiki_bookstack_mapping.csv` generated file to correspond to your needs. You can avoid uploading files by emptying the book name cell
5. Convert the Dokuwiki's pages: `dk2bs convert`
6. Upload Dokuwiki's pages to BookStack: `dk2bs upload`
7. Rewrite each Dokuwiki's pages to redirect to its corresponding BookStack page: `dk2bs redirect`

At any moment you can check the migration status with the following command: `dk2bs check`

```
Usage: dk2bs [OPTIONS] COMMAND [ARGS]...

Options:
  --version          Show the version and exit.
  -s, --source TEXT  Path to Dokuwiki's data source folder. Default:
                     ./dokuwiki_data
  -l, --lang TEXT    Setup CLI's language. Default: en
  --help             Show this message and exit.

Commands:
  check     Display migration state
  convert   Convert Dokuwiki's pages to HTML pages with integrated base64...
  prepare   Generate a CSV mapping file to be manually edited in order to...
  redirect  Modify each original Dokuwiki page to set a link to the new...
  upload    Create books and pages in BookStack from Dokuwiki's pages.
```

### Conversion

DK2BS uses Pandoc to convert Dokuwiki files to HTML.

DK2BS does following additional post-processing for each HTML page:

- For each `img` tag, use local images, or download them when a URL is given, to convert them into base64 and include them directly in the HTML files
- Optional: convert `WRAP` tags available through Dokuwiki's [plugin](https://www.dokuwiki.org/plugin:wrap). It is disabled by default because it is resource-consuming. Enable it with the `--convert-wrap` flag or the `DK2BS_CONVERT_WRAP='TRUE'` environment variable.

## Developpment

### Install dependencies

```
pip install -r requirements.txt
pip install -r requirements.dev.txt
```

### Run tests

```
make test
```

## License

[GPL 3.0](LICENSE)
