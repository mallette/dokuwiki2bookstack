from setuptools import setup

setup(
    name='Dokuwiki2BookStack',
    version='0.6.0',
    py_modules=['dk2bs'],
    install_requires=[
        'Click',
    ],
    include_package_data=True,
    license='GPL 3.0',
    packages=['dk2bs'],
    entry_points={
        'console_scripts': [
            'dk2bs = dk2bs.dk2bs:cli',
        ],
    },
)
