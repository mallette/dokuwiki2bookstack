from dk2bs.utils import prettify_name, slugify_name

def test_prettify_double_words_name():
    assert prettify_name('a_long_name') == 'A long name'

def test_slugify_double_words_name():
    assert slugify_name('His long book\'s name') == 'his-long-books-name'
