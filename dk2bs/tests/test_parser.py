from dk2bs.parser import process_html, post_process_html

FIXTURES_FOLDER = './dk2bs/tests/fixtures'

def test_process_html():
    """
    Check conversion from Dokuwiki file to HTML
    """
    output_file = '/tmp/output.html'
    status = process_html(f'{FIXTURES_FOLDER}/text.txt', output_file)
    # Check the conversion succeeded
    assert status == 0

    with open(output_file, 'r') as output:
        with open(f'{FIXTURES_FOLDER}/converted_text.html', 'r') as reference:
            # Check converted HTML is formattedas expected
            assert output.read() == reference.read()

def test_html_post_processing():
    """
    Check HTML images have been post-processed. Order with previous test matters.
    """
    output_file = '/tmp/output.html'
    content = post_process_html('text/text', output_file)

    with open(f'{FIXTURES_FOLDER}/processed_text.html', 'r') as reference:
        # Check converted HTML is formattedas expected
        assert content == reference.read()
