#!/bin/env python3
import base64
import re
import os
import logging
import subprocess

import requests
from bs4 import BeautifulSoup

import dk2bs.config as config
import dk2bs.runtime as runtime

logger = logging.getLogger(__name__)


def process_html(source_file, destination_file):
    """
    Convert Dokuwiki file to HTML
    """
    logger.debug(f"\nConverting {source_file} to {destination_file}")
    os.makedirs(os.path.dirname(destination_file), exist_ok=True)
    # Use Pandoc to convert dokuwiki files to html files
    result = subprocess.run([f"pandoc --from=dokuwiki {source_file} -o {destination_file}"], shell=True, capture_output=True)

    return result.returncode

def convert_dk_wrap(content):
    """
    Replace Dokuwiki's wraps by HTML callout tags known by BookStack.
    """

    def replace_wrap_type(matched_wrap):
        prepend_content = matched_wrap.group(1)
        wrap_type = matched_wrap.group(2)
        content = matched_wrap.group(3)

        # Set BookStack's callout class according to Dokuwiki's wrap class
        if wrap_type == 'important':
            callout_type = 'warning'
        elif wrap_type in ['info','tip','help','download']:
            callout_type = 'info'
        elif wrap_type == 'alert':
            callout_type = 'danger'
        return f'{prepend_content}<p class="callout {callout_type}">{content}</p>'

    # Replace WRAP tag
    search_string = re.compile(r"""
            (?:<p>)?(.*?)?                           # Take all content between
                                                     # a <p> and a WRAP tag
                                                     # as prepend content
            &lt;WRAP\s[\w\-]+\s[\w\-]+\s             # Match the <WRAP begin tag
            (important|info|tip|help|alert|download) # Match the WRAP's wrap_type
            \s[\w\-%]+&gt;
            (?:<br\s\/>)?(?:<\/p>)?\s?(?:<p>)?       # Remove potential <br />,
                                                     # <p>,</p> tags which
                                                     # follow directly a WRAP tag.
                                                     # Considered as noise.
            (.*?)                                    # Capture all the WRAP content
            (?:<p>)?                                 # Remove potiential <p>
                                                     # considered as noise inside
                                                     # the WRAP
            &lt;\/WRAP&gt;                           # Match the </WRAP> end tag
    """, re.DOTALL | re.VERBOSE)
    content = search_string.sub(replace_wrap_type, content)

    # Fix some p tags duplicates
    search_string = re.compile(r'<p><p\sclass="callout\s(warning|info|danger)">(.*?)</p></p>', re.DOTALL)
    content = search_string.sub(r'<p class="callout \1">\2</p>', content)
    return content

def convert_img_base64(content, destination_file, dw_page):
    """
    Parse all img tags in HTML and encode the src as base64.
    Retrieve the original file on the local storage or remotely.
    """
    try:
        soup = BeautifulSoup(content, 'html.parser')
    except Exception as e:
        runtime.ERRORS.append({'file': destination_file, 'context': 'Error while loading HTML file with BeautifulSoup', 'reason': e})

    for img_tag in soup.find_all('img'):
        logger.debug(f"\t\tConverting image to base64: {img_tag}")
        _, img_format = os.path.splitext(img_tag['src'])
        img_format = img_format.replace('.', '') # Remove the . in the extension

        img_path = f"{config.DOKUWIKI_DATA}/media/{os.path.dirname(dw_page)}/{img_tag['src']}"

        # Sometimes there are already relative paths in src. Use them directly
        if "//" in img_path:
            img_path = f"{config.DOKUWIKI_DATA}/media{img_tag['src']}"

        # Ensure the path is in lowercase
        img_path = img_path.lower()

        img = ''
        # Sometimes an absolute Internet link can be set
        # Request the image remotely in this case
        if "http://" in img_path or "https://" in img_path:
            img_path = img_tag['src']
            try:
                img = requests.get(img_tag['src']).content
            except Exception as e:
                runtime.ERRORS.append({'file': destination_file, 'context': f"Error while requesting image file with src {img_tag['src']}", 'reason': e})
        else:
            # Load image from local filesystem
            try:
                with open(img_path, 'rb') as img_file:
                    img = img_file.read()
            except Exception as e:
                runtime.ERRORS.append({'file': destination_file, 'context': f"Error while opening image file with src {img_tag['src']}", 'reason': e})

        # Encode image in base64
        try:
            base64_img = base64.b64encode(img).decode('utf-8')
            img_tag['src'] = f"data:image/{img_format};base64,{base64_img}"
        except Exception as e:
            runtime.ERRORS.append({'file': destination_file, 'context': 'Error while converting image file to base64', 'reason': e})

    return str(soup)

def post_process_html(dw_page, destination_file):
    """
    Post-process the HTML file generated by Pandoc.
    Replace Dokuwiki's wraps and base64 encode images.
    """

    # Read HTML file to do post-processing
    with open(destination_file, 'r') as html_file:
        content = html_file.read()

        if config.ENV['DK2BS_CONVERT_WRAP'] == True:
            # Convert WRAP tags from Dokuwiki's plugin to BookStack's callout
            content = convert_dk_wrap(content)

        # Convert images to base64
        content = convert_img_base64(content, destination_file, dw_page)

        # Add EOF if needed
        if content[-1] != "\n":
            content += '\n'

        return content
