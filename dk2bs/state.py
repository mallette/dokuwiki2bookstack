#!/bin/env python3
import os
import csv

import click
import requests

import dk2bs.config as config
import dk2bs.runtime as runtime
from .utils import slugify_name

def get_dw_pages():
    """
    List all Dokuwiki's pages in data folder and store them in a dict variable.
    """
    dw_pages = {}
    for dp, dn, filenames in os.walk(f'{config.DOKUWIKI_DATA}/pages'):
        for f in filenames:
            dw_page_path = os.path.join(dp, f).replace(f'{config.DOKUWIKI_DATA}/pages/', '').replace('.txt', '')
            dw_pages[dw_page_path] = {'status': 'ignored'}
    return dw_pages

def load_mapping_state(dw_pages, filter=None):
    """
    Load Dokuwiki-BookStack mapping file and prepare BookStack pages dict
    """
    click.secho('Loading mapping file...', bold=True)

    books_pages = {}

    # Split page filter string
    if filter is not None:
        book_name, page_name = filter.split('/', 2)

    with open(config.DOKUWIKI_BOOKSTACK_MAPPING_FILE, 'r') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        for row in csv_reader:
            # Filter on one page if the filter is defined
            if ((filter is not None
                    and row['bookstack_book_destination'] == book_name
                    and row['bookstack_page_name'] == page_name)
                or filter is None):
                # Do not manage empty destination columns
                if row['bookstack_book_destination'] != '':
                    # Update DW pages status
                    try:
                        dw_pages[row['dokuwiki_path']]['status'] = 'mapped'
                    except KeyError as e:
                        runtime.ERRORS.append({'file': row['dokuwiki_path'], 'context': 'Checking Dokuwiki\'s page from mapping failed', 'reason': e})

                    # Load BookStack's target status
                    if row['bookstack_book_destination'] not in books_pages:
                        books_pages[row['bookstack_book_destination']] = {'id': None, 'slug': None, 'pages': {}}
                    books_pages[row['bookstack_book_destination']]['pages'][slugify_name(row['bookstack_page_name'])] = {
                        'id': None,
                        'page_name': row['bookstack_page_name'],
                        'dokuwiki_path': row['dokuwiki_path'],
                        'converted': None,
                        'created_by': None,
                        'updated_by': None,
                    }

    return books_pages

def load_bookstack_state():
    """
    Fetch BookStack's current data about shelves, books and pages
    """
    click.secho('Loading Bookstack state...', bold=True)
    try:
        resp = requests.get(f"{config.API_PREFIX}/books", headers=config.BS_HEADERS)
        if resp.status_code != 200 and resp.status_code != 204:
            raise Exception(f"Error while requesting BookStack's API: {resp.json()['error']}")
        available_books = resp.json()['data']

        resp = requests.get(f"{config.API_PREFIX}/shelves", headers=config.BS_HEADERS)
        if resp.status_code != 200 and resp.status_code != 204:
            raise Exception(f"Error while requesting BookStack's API: {resp.json()['error']}")
        available_shelves = resp.json()['data']

        available_pages = []
        if config.FLAG_CHECK_PAGE_DETAILS:
            click.secho('Loading additionnal Bookstack pages status...', bold=True)
            resp = requests.get(f"{config.API_PREFIX}/pages?count=500", headers=config.BS_HEADERS)
            if resp.status_code != 200 and resp.status_code != 204:
                raise Exception(f"Error while requesting BookStack's API: {resp.json()['error']}")
            available_pages += resp.json()['data']
            total_nb_pages = resp.json()['total']
            while len(available_pages) < total_nb_pages:
                resp = requests.get(f"{config.API_PREFIX}/pages?count=500&offset={len(available_pages)}", headers=config.BS_HEADERS)
                if resp.status_code != 200 and resp.status_code != 204:
                    raise Exception(f"Error while requesting BookStack's API: {resp.json()['error']}")
                available_pages += resp.json()['data']

    except Exception as e:
        raise Exception(f'Encountered an issue while requesting Bookstack\'s API. Maybe the API is unavailable: {e}')

    return available_shelves, available_books, available_pages

def associate_state_and_mapping(books_pages, available_books, available_pages):
    """
    Associate BookStack's current state with the mapping file.
    Load the details in the given books_pages dict.
    """
    for book in available_books:
        # Load available book to mapping
        if book['name'] in books_pages:
            book_map = books_pages[book['name']]
            book_map['id'] = book['id']
            book_map['slug'] = book['slug']
            # Get book details
            try:
                resp = requests.get(f"{config.API_PREFIX}/books/{book['id']}", headers=config.BS_HEADERS)
                if resp.status_code != 200 and resp.status_code != 204:
                    raise Exception(f"Error while requesting BookStack's API: {resp.json()['error']}")
            except Exception as e:
                raise Exception(f'Encountered an issue while requesting Bookstack\'s API. Maybe the API is unavailable: {e}')

            book = resp.json()
            for page in book['contents']:
                # Load available page to mapping
                if page['slug'] in book_map['pages']:
                    book_map['pages'][page['slug']]['id'] = page['id']

                # Search pages through chapters
                if page['type'] == 'chapter':
                    for sub_page in page['pages']:
                        if sub_page['slug'] in book_map['pages']:
                            book_map['pages'][sub_page['slug']]['id'] = sub_page['id']

            # Load additionnal page data
            for page in available_pages:
                if page['book_slug'] == book['slug']:
                    if page['slug'] in book_map['pages']:
                        book_map['pages'][page['slug']]['created_by'] = page['created_by']
                        book_map['pages'][page['slug']]['updated_by'] = page['updated_by']

    for book, book_data in books_pages.items():
        for page_key, page in book_data['pages'].items():
            page_path = f"{config.WORKDIR}/{page['dokuwiki_path']}.html"
            is_page_converted = os.path.exists(page_path)
            page['converted'] = is_page_converted
            if not is_page_converted:
                runtime.ERRORS.append({'file': page['page_name'], 'context': 'HTML page doesn\'t exists', 'reason': None})

    return books_pages
