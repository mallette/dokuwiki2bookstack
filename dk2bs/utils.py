#!/bin/env python3
import os

import click
import i18n

import dk2bs.config as config


def prettify_name(name):
    name = name.replace('_', ' ')
    name = name.capitalize()

    return name

def slugify_name(name):
    name = name.lower()
    name = name.replace('_', '-')
    name = name.replace(' ', '-')
    name = name.replace('\'', '')

    return name

def check_data_setup(ctx, param, source):
    """
    Check that Dokuwiki's data folder exists.
    """
    data_folder = source if source else config.DOKUWIKI_DATA
    if not os.path.isdir(data_folder):
        click.echo(f"Dokuwiki's source data folder doesn't exist: {source}")
        ctx.abort()

def setup_lang(ctx, param, lang):
    """
    Setup language according to environment variables and parameter
    """
    # Set CLI's language based on LANGUAGE environment variable
    for env_lang in os.environ['LANGUAGE'].split(':'):
        if env_lang in config.AVAILABLE_LANGUAGES:
            i18n.set('locale', env_lang)
            break

    # Set CLI's language if specified
    if lang in config.AVAILABLE_LANGUAGES:
        i18n.set('locale', lang)
