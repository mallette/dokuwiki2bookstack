#!/bin/env python3
from datetime import datetime
import csv
import logging
import os

import requests
import click
import i18n

import dk2bs.config as config
import dk2bs.runtime as runtime
import dk2bs.utils as utils
from .parser import process_html, post_process_html
from .state import get_dw_pages, load_mapping_state, load_bookstack_state, associate_state_and_mapping

__PACKAGE_NAME__ = 'Dokuwiki2BookStack'

logger = logging.getLogger(__name__)
logger.setLevel(config.ENV['APP_LOGGING_LEVEL'])

c_handler = logging.StreamHandler()
c_format = logging.Formatter('%(levelname)s | %(message)s')
c_handler.setFormatter(c_format)
logger.addHandler(c_handler)


@click.group()
@click.version_option(package_name=__PACKAGE_NAME__)
@click.option('--source', '-s', callback=utils.check_data_setup, expose_value=True, help=f"Path to Dokuwiki's data source folder. Default: {config.DOKUWIKI_DATA}")
@click.option('--lang', '-l', callback=utils.setup_lang, expose_value=True, help=f"Setup CLI's language. Default: {config.DEFAULT_LANGUAGE}")
def cli(source, lang):
    pass


@cli.command('prepare')
def prepare_migration():
    """
    Generate a CSV mapping file to be manually edited in order to sort Dokuwiki's
    pages with their Bookstack destination.
    """
    # Copy CSV template
    if os.path.isfile(config.DOKUWIKI_BOOKSTACK_MAPPING_FILE):
        click.confirm('A mapping file already exist. Do you want to override it?', abort=True)

    with open(config.DOKUWIKI_BOOKSTACK_MAPPING_FILE, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        writer.writerow([
            'dokuwiki_path',
            'dokuwiki_category',
            'bookstack_book_destination',
            'notes',
            'bookstack_page_name'
        ])
        # List in CSV all Dokuwiki's pages
        dw_pages = get_dw_pages()
        for dw_page, value in dw_pages.items():
            if os.path.dirname(dw_page):
                book_name = utils.prettify_name(os.path.basename(os.path.dirname(dw_page)))
            else:
                book_name = 'Home'
            writer.writerow([
                dw_page,
                os.path.dirname(dw_page),
                book_name,
                '',
                utils.prettify_name(os.path.basename(dw_page))
            ])


@cli.command('check')
def display_state(page=None):
    """
    Display migration state
    """
    migration_state = check_state(page)

    click.secho('Migation status:', bold=True)
    for book, content in migration_state['bs_books'].items():
        creation_state = click.style('created' if content['id'] else 'not created', fg=('green' if content['id'] else 'red'))
        click.echo(f"\t{book} [{creation_state}]")
        for page_key, page in content['pages'].items():
            page_status = f"No page status data"
            if page['id']:
                conversion_state = click.style('created', fg='green')
                if page['created_by'] and page['updated_by']:
                    page_status = f"created_by: {page['created_by']} updated_by: {page['updated_by']}"
            elif page['converted']:
                conversion_state = click.style('converted', fg='blue')
            else:
                conversion_state = click.style('not converted', fg='red')
            if config.FLAG_CHECK_PAGE_DETAILS:
                click.echo(f"\t\t{utils.prettify_name(page['page_name'])} [{conversion_state}] [{page_status}]")
            else:
                click.echo(f"\t\t{utils.prettify_name(page['page_name'])} [{conversion_state}]")

    click.secho('Ignored Dokuwiki\'s pages:', bold=True)
    for page, content in migration_state['dw_pages'].items():
        if content['status'] == 'ignored':
            click.echo(f"\t{page}")

    click.echo(f"\nNumber of errors: {click.style(len(migration_state['errors']), fg='red')}")
    for error in migration_state['errors']:
        click.echo(f"\t{error['file']}: {error['context']}\n\t\t{error['reason']}")

    return migration_state


def check_state(page=None):
    """
    Check Bookstack API and Dokuwiki's mapping file to display the difference
    between the content of both systems.
    """

    # Check Dokuwiki's data
    click.secho(f'Loading Dokuwiki\'s pages located in {config.DOKUWIKI_DATA}...', bold=True)
    dw_pages = get_dw_pages()

    # Load CSV file in books dict
    books_pages = load_mapping_state(dw_pages, filter=page)

    # Load BookStack's state
    available_shelves, available_books, available_pages = load_bookstack_state()

    # Associate the mapping file with BookStack's data
    books_pages = associate_state_and_mapping(books_pages, available_books, available_pages)

    migration_state = {
        'date': datetime.now().isoformat(),
        'bs_books': books_pages,
        'dw_pages': dw_pages,
        'errors': runtime.ERRORS
    }
    return migration_state


@cli.command('convert')
@click.pass_context
@click.option('--yes', is_flag=True, help="Avoid confirmation prompt")
@click.option('--convert-wrap/--no-convert-wrap', is_flag=True, default=False, help="Convert wrap tags from Dokuwiki plugin")
@click.option('--file', '-f', multiple=True, help="Dokuwiki's page path in working directory without extension")
def convert_dw_to_bk(ctx, yes, convert_wrap, file):
    """
    Convert Dokuwiki's pages to HTML pages with integrated base64 images
    ready to be pushed to Bookstack.
    """
    # Create workdir folder if it doesn't already exists
    os.makedirs(config.WORKDIR, exist_ok=True)

    dw_pages = ctx.invoke(display_state)['dw_pages']

    if not yes:
        click.confirm('Do you want to continue?', abort=True)

    # Take into account wrap conversion option
    if convert_wrap:
        config.ENV['DK2BS_CONVERT_WRAP'] = True

    click.echo('Converting Dokuwiki files to HTML...')

    # Convert each dokuwiki page to HTML
    if file:
        pages_to_convert = file
    else:
        pages_to_convert = [page for page in dw_pages.keys() if dw_pages[page]['status'] == 'mapped']
    with click.progressbar(pages_to_convert, label='Converting...') as bar:
        for dw_page in bar:
            source_file = f"{config.DOKUWIKI_DATA}/pages/{dw_page}.txt"
            destination_file = f"{config.WORKDIR}/{dw_page}.html"
            result = process_html(source_file, destination_file)
            # Continue conversion only if HTML conversion has succeeded
            if result == 0:
                content = post_process_html(dw_page, destination_file)

                # Write updated file
                with open(destination_file, 'w') as html_file:
                    html_file.write(content)
            else:
                # Store errors to display at the end
                runtime.ERRORS.append({'file': source_file, 'context': 'Error while converting to HTML with Pandoc', 'reason': result.stderr})

    click.echo(f"Number of conversion errors: {len(runtime.ERRORS)}")
    for error in runtime.ERRORS:
        click.echo(f"\t{error['file']}: {error['context']}\n\t\t{error['reason']}")

@cli.command()
@click.pass_context
@click.option('--yes', is_flag=True, help="Avoid confirmation prompt")
@click.option('--rewrite', is_flag=True, help="Reupload only pages which haven't been modified by other user accounts")
@click.option('--page', '-p', multiple=False, help="Upload only this BookStack book and page destinations in form of 'Book Title/Page name'")
def upload(ctx, yes, rewrite, page):
    """
    Create books and pages in BookStack from Dokuwiki's pages.
    """
    # Load page details required for rewriting
    if rewrite:
        config.FLAG_CHECK_PAGE_DETAILS = True

    migration_state = ctx.invoke(display_state, page=page)

    if not yes:
        click.confirm("Are you sure you want to upload the pages?", abort=True)

    click.secho("Starting uploading to BookStack...", bold=True)

    for book, book_data in migration_state['bs_books'].items():
        if not book_data['id']:
            click.echo(f"Creating book {book}")
            try:
                resp = requests.post(
                    f"{config.API_PREFIX}/books",
                    headers=config.BS_HEADERS,
                    data={'name': book}
                )
                book_data['id'] = resp.json()['id']
            except Exception as e:
                runtime.ERRORS.append({'object': book, 'context': "Error while creating book", 'reason': e})
        for page_key, page in book_data['pages'].items():
            page_name = utils.prettify_name(page['page_name'])
            page_file = f"{config.WORKDIR}/{page['dokuwiki_path']}.html"

            # When rewrite, upload only existing pages which have been last modified
            # by this same script
            if rewrite:
                if page['id'] and page['updated_by'] == page['created_by'] and page['created_by'] is not None:
                    click.echo(f"\tUpdating page {page_name}")
                    # Read HTML file and convert images to base64
                    with open(page_file, 'r') as html_file:
                        try:
                            resp = requests.put(
                                f"{config.API_PREFIX}/pages/{page['id']}",
                                headers=config.BS_HEADERS,
                                data={
                                    'html': html_file.read()
                                }
                            )
                        except Exception as e:
                            runtime.ERRORS.append({'object': page_name, 'context': "Error while updating page", 'reason': e})
            # When normal mode, only upload pages which do not exist yet
            elif not page['id']:
                click.echo(f"\tCreating page {page_name}")
                # Read HTML file and convert images to base64
                with open(page_file, 'r') as html_file:
                    try:
                        resp = requests.post(
                            f"{config.API_PREFIX}/pages",
                            headers=config.BS_HEADERS,
                            data={
                                'name': page_name,
                                'book_id': book_data['id'],
                                'html': html_file.read()
                            }
                        )
                        page['id'] = resp.json()['id']
                    except Exception as e:
                        runtime.ERRORS.append({'object': page_name, 'context': "Error while creating page", 'reason': e})

    click.echo(f"Number of errors: {len(runtime.ERRORS)}")
    for error in runtime.ERRORS:
        if 'object' in error:
            click.echo(f"\t{error['object']}: {error['context']}\n\t\t{error['reason']}")

    click.secho("Finished uploading to BookStack", bold=True)


@cli.command('redirect')
@click.pass_context
@click.option('--yes', is_flag=True, help="Avoid confirmation prompt")
@click.option('--replace', is_flag=True, help="Replace whole content with redirection")
def add_redirections(ctx, yes, replace):
    """
    Modify each original Dokuwiki page to set a link to the new BookStack page.
    """
    migration_state = ctx.invoke(display_state)

    if not yes:
        click.confirm("Are you sure you want to modify Dokuwiki's pages?", abort=True)

    click.secho("Starting modifying Dokuwiki's pages...", bold=True)

    with click.progressbar(migration_state['bs_books'].items(), label='Writing...') as bar:
        for book, book_data in bar:
            for page_key, page in book_data['pages'].items():
                bs_page_url = f"{config.ENV['BOOKSTACK_URL']}/books/{book_data['slug']}/page/{page_key}"
                source_file = f"{config.DOKUWIKI_DATA}/pages/{page['dokuwiki_path']}.txt"
                logger.debug(f"Rewriting {source_file} to add a redirect to {bs_page_url}")
                content = ''

                with open(source_file, 'r') as dw_page:
                    content = dw_page.read()

                with open(source_file, 'w') as dw_page:
                    redirection_message = i18n.t('translate.redirection_message', bs_page_url=bs_page_url, page_name=page_key)
                    # Replace whole content with a redirection to the new page
                    if replace:
                        content = redirection_message
                    # Otherwise append legacy content to page by default
                    else:
                        content = f"{redirection_message}\n\n{content}"
                    dw_page.write(content)

    click.secho("Finished modifying Dokuwiki's pages.", bold=True)

if __name__ == '__main__':
    cli()
