#!/bin/env python3
import os
import glob

from dotenv import load_dotenv
import i18n

# Load the stored environment variables
load_dotenv()
ENV = dict(os.environ)

# Normalize envvar strings to boolean
for key, value in ENV.items():
    if value.lower() == 'true':
        ENV[key] = True
    elif value.lower() == 'false':
        ENV[key] = False

## CONSTANTS SETUP
DOKUWIKI_DATA=ENV.get('DOKUWIKI_DATA', "./dokuwiki_data")
WORKDIR=ENV.get('WORKDIR', "./workdir")
CURRENT_WORKDIR = os.getcwd()

DOKUWIKI_BOOKSTACK_MAPPING_FILE="dokuwiki_bookstack_mapping.csv"
API_PREFIX=f"{ENV['BOOKSTACK_URL']}/api"
BS_HEADERS = {'Authorization': f"Token {ENV['BOOKSTACK_ACCESS_TOKEN']}:{ENV['BOOKSTACK_SECRET_TOKEN']}"}

## I18N
# Setup languages configuration
lang_folder_path = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'lang')
i18n.load_path.append(lang_folder_path)

# Parse available languages in folder
language_list = glob.glob("{}/*.json".format(lang_folder_path))

def convert_file_paths_to_lang(file_path):
    filename  = os.path.basename(file_path)
    lang, ext = os.path.splitext(filename)
    # Split to remove namespace from file name
    return lang.split('.')[1]

AVAILABLE_LANGUAGES = list(map(convert_file_paths_to_lang, language_list))
DEFAULT_LANGUAGE='en'
i18n.set('fallback', DEFAULT_LANGUAGE)

## FLAGS
# Set flag to fetch page details
FLAG_CHECK_PAGE_DETAILS = False
