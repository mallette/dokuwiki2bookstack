# Run all recipe lines as one shell
.ONESHELL:

all: help

DOCKER_IMAGE_TAG ?= 'latest'

UNAME_S := $(shell uname -s)
# GNU/Linux configuration
ifeq ($(UNAME_S),Linux)
	DOCKER_COMPOSE = DOCKER_IMAGE_TAG=$(DOCKER_IMAGE_TAG) DOCKER_UID=$(shell id -u) DOCKER_GID=$(shell id -g) docker compose
endif
# Mac OS configuration
ifeq ($(UNAME_S),Darwin)
	DOCKER_COMPOSE = DOCKER_IMAGE_TAG=$(DOCKER_IMAGE_TAG) DOCKER_UID=$(shell id -u) DOCKER_GID=3000 docker compose
endif

.PHONY: help
help:
	@echo 'Available targets:'
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'
	@printf '\nAvailable variables:\n'
	@grep -E '^[a-zA-Z_-]+\?=.* ## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = "?=.* ## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

.PHONY: changelog
changelog: ## Display changelog since last merge
	@echo "Features"
	@git log --oneline --no-merges --grep "^feat" $$(git describe --tags --abbrev=0)..HEAD
	@echo "Bug Fixes"
	@git log --oneline --no-merges --grep "^fix" $$(git describe --tags --abbrev=0)..HEAD

.PHONY: check_pandoc_version
check_pandoc_version: ## Check pandoc version if meets requirements
	bash lib/check_pandoc_version.sh

.PHONY: install_pandoc
install_pandoc: ## Install the Pandoc binary in /usr/local/bin
	curl -L https://github.com/jgm/pandoc/releases/download/3.1.13/pandoc-3.1.13-linux-amd64.tar.gz -o lib/pandoc.tar.gz
	tar -xf lib/pandoc.tar.gz -C lib
	sudo cp lib/pandoc-3.1.13/bin/pandoc /usr/local/bin

.env:
	cp .env.template .env

.PHONY: dev_config
dev_config: .env ## Configure app to be run locally with dev tools
	echo "COMPOSE_FILE='./docker-compose.dev.yml'" >> .env

.PHONY: build
build: ## Build containers
	$(DOCKER_COMPOSE) build

.PHONY: docker_test
docker_test: ## Run Python tests through Docker
	$(DOCKER_COMPOSE) run --rm cli pytest -vv

.PHONY: test
test: ## Run Python tests
	. .venv/bin/activate
	pytest -vv --log-level=DEBUG

.PHONY: setup_venv
setup_venv: ## Setup venv
	python3 -m venv .venv
	. .venv/bin/activate
	pip install -r requirements.txt

.PHONY: setup_dev_env
setup_dev_env: setup_venv .env ## Install dev dependencies
	. .venv/bin/activate
	pip install -r requirements.dev.txt
