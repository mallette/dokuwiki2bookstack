# Changelog

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## v0.5.0 (2024-04-18)

### Features
- 8f09352 feat(cli): Set wrap conversion behind an option
- be20efb feat: Replace WRAP tags at post-processing

### Bug Fixes

- cd26691 fix(parser): Manage isolated WRAP tag
- 7caf540 fix(parser): Remove br at beginning of WRAP
- 2a07e87 fix(upload): Add missing config variable
- 8c138d1 fix(parser): Remove duplicate p tags
- 830a46c fix: Add missing requests import

## v0.4.0 (2024-04-10)

### Features

- 71fa14c feat: Load page details only when rewrite flag is given
- 0a3aa81 feat: Add rewrite upload option
- 15b34c2 feat: Display created_by and updated_by in check
- 1e36ded feat: Search pages through chapters
- e51d8de feat: Change and update Pandoc installation

### Bug Fixes

- 77fcbef fix: Add missing BookStack error management

## v0.3.1 (2024-04-03)

### Bug fixes

- b99d8c7 fix: Manage BookStack's API HTTP codes
- 30ec9ab fix(redirect): Use page slug for path redirection

## v0.3.0 (2024-04-02)

### Features

- 50f245c feat: Add progress bar while rewriting source pages
- 31aa9a2 feat: Translate CLI's strings
- fcfd522 feat(redirect): Add redirection message in dokuwiki's pages

## v0.2.0 (2024-03-27)

### Features

- a239e43 feat(convert): Take wrap's type into consideration
